import { SvelteComponentTyped } from "svelte";
import * as echarts from 'echarts';
export { echarts };
export declare type EChartsOptions = echarts.EChartsOption;
export declare type EChartsTheme = string | object;
export declare type EChartsRenderer = 'canvas' | 'svg';
export declare type ChartOptions = {
    theme?: EChartsTheme;
    renderer?: EChartsRenderer;
    options: EChartsOptions;
};
export declare function chartable(element: HTMLElement, echartOptions: ChartOptions): {
    destroy(): void;
    update(newOptions: ChartOptions): void;
};
declare const __propDef: {
    props: {
        options: echarts.EChartsOption;
        theme?: EChartsTheme;
        renderer?: EChartsRenderer;
    };
    events: {
        [evt: string]: CustomEvent<any>;
    };
    slots: {};
};
export declare type ChartProps = typeof __propDef.props;
export declare type ChartEvents = typeof __propDef.events;
export declare type ChartSlots = typeof __propDef.slots;
export default class Chart extends SvelteComponentTyped<ChartProps, ChartEvents, ChartSlots> {
}
